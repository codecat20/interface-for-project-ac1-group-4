let aud = ['b.wav', 'B1.wav', 'B12.wav', 'B13.wav', 'B14.wav', 'B17.wav', 'B19.wav', 'B23.wav', 'B25.wav', 'B26.wav', 'B3.wav', 'B31.wav', 'B32.wav', 'B34.wav', 'B35.wav', 'B38.wav', 'B4.wav', 'B41.wav', 'B43.wav', 'B45.wav', 'B46.wav', 'B49.wav', 'B7.wav',
  'c.wav', 'C13.wav', 'C15.wav', 'C2.wav', 'C20.wav', 'C21.wav', 'C22.wav', 'C23.wav', 'C25.wav', 'C26.wav', 'C27.wav', 'C29.wav', 'C3.wav', 'C32.wav', 'C33.wav', 'C34.wav', 'C37.wav', 'C42.wav', 'C45.wav', 'C46.wav', 'C49.wav', 'C5.wav', 'C7.wav',
  'k.wav', 'K1.wav', 'K10.wav', 'K12.wav', 'K19.wav', 'K2.wav', 'K27.wav', 'K29.wav', 'K3.wav', 'K33.wav', 'K34.wav', 'K4.wav', 'K40.wav', 'K41.wav', 'K42.wav', 'K43.wav', 'K46.wav', 'K47.wav', 'K48.wav', 'K5.wav', 'K6.wav', 'K7.wav', 'K9.wav',
  's.wav', 'S10.wav', 'S11.wav', 'S13.wav', 'S14.wav', 'S16.wav', 'S18.wav', 'S21.wav', 'S22.wav', 'S27.wav', 'S30.wav', 'S34.wav', 'S37.wav', 'S39.wav', 'S4.wav', 'S40.wav', 'S42.wav', 'S44.wav', 'S45.wav', 'S46.wav', 'S47.wav', 'S8.wav', 'S9.wav']

let vis = ['a.png', 'e.png', 'i.png', 'o.png',
  'J1.png', 'J10.png', 'J11.png', 'J12.png', 'J13.png', 'J14.png', 'J15.png', 'J16.png', 'J17.png', 'J18.png', 'J19.png', 'J2.png', 'J20.png', 'J3.png', 'J4.png', 'J5.png', 'J6.png', 'J7.png', 'J8.png', 'J9.png',
  'P1.png', 'P10.png', 'P11.png', 'P12.png', 'P13.png', 'P14.png', 'P15.png', 'P16.png', 'P17.png', 'P18.png', 'P19.png', 'P2.png', 'P20.png', 'P21.png', 'P22.png', 'P23.png', 'P24.png', 'P3.png', 'P4.png', 'P5.png', 'P6.png', 'P7.png', 'P8.png', 'P9.png',
  'W1.png', 'W10.png', 'W11.png', 'W12.png', 'W13.png', 'W14.png', 'W15.png', 'W16.png', 'W17.png', 'W18.png', 'W19.png', 'W2.png', 'W20.png', 'W21.png', 'W22.png', 'W3.png', 'W4.png', 'W5.png', 'W6.png', 'W7.png', 'W8.png', 'W9.png']

const shuffle = (v) => jsPsych.randomization.shuffle(v)

let fixation = {
  type: "html-keyboard-response",
  stimulus: `
    <div style="padding-bottom: 12em">
      <img src="Fixation.png" />
    </div>
    `,
  choices: jsPsych.NO_KEYS,
  data: { type: "fixation" },
  trial_duration: () => Math.random() * 500 + 500
}

const rest = {
  type: 'html-keyboard-response',
  stimulus: `
    <p>You completed a section! Take a break if you want.</p>
    <p>Press Space to continue</p>
  `,
  data: { type: "rest" },
  choices: ['space']
}

let proceed = {
  type: 'html-keyboard-response',
  stimulus: `
    <p> You completed the trials! </p>
    <p> Press Space to proceed to the questionnaires. </p>
  `,
  choices: [
    "space"
  ]
}

document.addEventListener('keypress', (e) => {
  const trial = jsPsych.currentTrial()
  console.log({ trial })
  if (e.key === 'r' && trial.data.type !== 'visual' && trial.data.response !== 'none') {
    const audio = document.getElementById('audio-player')
    if (audio) {
      audio.currentTime = 0
      audio.play()
    }
  }
})

const plugin_type = 'html-keyboard-response'
const makeStimulus = (type, v) => type === 'auditory' ?
  `<audio src="${'aud/' + v}" id="audio-player"/>`
  : `<img style="height: 20em; border: 1px solid black" src="${'vis/' + v}">`

const num = v => `<p><strong>${v}</strong></p>`
const numbers = `<div style="display: grid; grid-template-columns: 1fr 1fr 1fr 1fr 1fr; width: 30em">${[1, 2, 3, 4, 5].map(num).join(' ')}</div>`
const makeChoices = (left = '', right = '') => `
<div style="display: grid; grid-template-columns: 1fr 3fr 1fr"><p>${left}</p> ${numbers} <p>${right}</p></div>
`

const loop_function = (type, data) =>
  type === 'auditory' && jsPsych.pluginAPI.convertKeyCharacterToKeyCode('r') === data.values()[0].key_press

const audPrompt = type => type === 'auditory' ? `
  <hr />
  <p>Press R to replay the piece</p>
  `: ''

const choices = ["1", "2", "3", "4", "5"]
const makeTrial = (type) => (v) => {
  const stimulus = makeStimulus(type, v)
  const liking = {
    timeline: [
      {
        stimulus,
        data: { type: v === v.toLowerCase() ? 'test' : type, response: 'liking' },
        prompt: `
        <div>
          <h3>How much do you like this?</h3>
          <div style="height: 5.5em"></div>
          ${makeChoices('(Not at all)', '(Very much)')}
          ${audPrompt(type)}
        </div>
      `,
        type: plugin_type,
        choices
      }
    ],
    loop_function: data => loop_function(type, data)
  }

  const others = [
    {
      timeline: [
        {
          stimulus,
          data: { type: v === v.toLowerCase() ? 'test' : type, response: 'valence' },
          prompt: `
            <div>
              <h3>How do you perceive it?</h3>
              <img src="valence.PNG" style="height: 5em" />
              ${makeChoices()}
              ${audPrompt(type)}
            </div>
          `,
          type: plugin_type,
          choices,
        }
      ],
      loop_function: data => loop_function(type, data)
    },
    {
      timeline: [
        {
          stimulus,
          data: { type: v === v.toLowerCase() ? 'test' : type, response: 'arousal' },
          prompt: `
          <div>
            <h3>How do you perceive it?</h3>
            <img src="arousal.PNG" style="height: 5em" />
            ${makeChoices()}
            ${audPrompt(type)}
          </div>
        `,
          type: plugin_type,
          choices,
        },
      ],
      loop_function: data => loop_function(type, data)
    }
  ]

  return {
    timeline: [
      ...(type !== 'auditory' ? [fixation] : [
        {
          type: plugin_type,
          data: { type: 'auditory', response: 'none' },
          prompt: `
            <h3>Please listen to the piece</h3>
          `,
          choices: jsPsych.NO_KEYS,
          trial_duration: 4500,
          stimulus: `<audio src="${'aud/' + v}" autoplay/>`
        }
      ]),
      ...shuffle([liking, others]).flat(Infinity)
    ],
    data: { type: v === v.toLowerCase() ? 'test' : type, block: v.slice(0, 1) }
  }
}

let aud_vars = aud.map(makeTrial('auditory'))
let vis_vars = vis.map(makeTrial('visual'))

const practice_start = {
  type: plugin_type,
  data: { type: 'test-start' },
  prompt: `
    <h3>This is the start of the practice round.</h3>
    <p>Press Space to continue</p>
  `,
  stimulus: '<div></div>',
  choices: ['space']
}

const practice_end = {
  type: plugin_type,
  data: { type: 'test-start' },
  prompt: `
    <h3>This is the end of the practice round.</h3>
    <p>Press Space to continue</p>
  `,
  stimulus: '<div></div>',
  choices: ['space']
}

let vis_practice = [practice_start, ...shuffle(vis_vars.filter(v => v.data.type === 'test')), practice_end]
let aud_practice = [practice_start, ...shuffle(aud_vars.filter(v => v.data.type === 'test')), practice_end]

let format = (arr, b) => {
  const block = shuffle(arr.filter(v => v.data.block === b))
  return [
    ...block,
    rest
  ]
}

const aud_instructions = {
  type: 'html-keyboard-response',
  stimulus: `<div>
    <p> In this part of the experiment, you will listen to some short pieces of music. When you hear each piece, please specify </p>
    <p> (1) How much do you like or dislike the piece? </p>
    <p> (2) How negative or positive do you feel the piece is? </p>
    <p> (3) How arousing do you feel the piece is? </p>
    <p> Arousal in this context means the physiological and psychological state of being awake,
      or of your sensory organs being stimulated to the point of perception.</p>
    <p> You can do this by pressing the keys from 1 to 5 on the keyboard (corresponding to the scale you will see on the screen). </p>
    <p> If you would like to hear the piece of music again, press R on the keyboard. </p>
    <hr />
    <p> Press any key to continue. </p>
  </div>`,
  data: { type: 'instructions' }
}

const vis_instructions = {
  type: 'html-keyboard-response',
  stimulus: `<div>
  <p> In this part of the experiment, you will look at some images. When you have examined each image, please specify </p>
  <p> (1) How much do you like or dislike the image? </p>
  <p> (2) How negative or positive do you feel the image is? </p>
  <p> (3) How arousing do you feel the image is? </p>
  <p> Arousal in this context means the physiological and psychological state of being awake,
    or of your sensory organs being stimulated to the point of perception.</p>
  <p> You can do this by pressing the keys from 1 to 5 on the keyboard (corresponding to the scale you will see on the screen). </p>
  <p> You may look at the image for as long as you need. </p>
  <hr />
  <p> Press any key to continue. </p>
</div>`,
  data: { type: 'instructions' }
}

let vis_J = format(vis_vars, "J")
let vis_P = format(vis_vars, "P")
let vis_W = format(vis_vars, "W")

let aud_B = format(aud_vars, "B")
let aud_C = format(aud_vars, "C")
let aud_K = format(aud_vars, "K")
let aud_S = format(aud_vars, "S")

let vis_blocks = shuffle([vis_J, vis_P, vis_W])
let aud_blocks = shuffle([aud_B, aud_C, aud_K, aud_S])
let combined_blocks = shuffle([[
  aud_instructions,
  aud_practice,
  ...aud_blocks,
], [
  vis_instructions,
  vis_practice,
  ...vis_blocks
]])

let blocks = [
  combined_blocks[0],
  combined_blocks[1]
]

let welcome = {
  type: "html-keyboard-response",
  stimulus: "Welcome to the experiment. Press any key to begin."
}

const scale_cognition = [
  "Very strongly disagree",
  "Strongly disagree",
  "Moderately disagree",
  "Slightly disagree",
  "Neither agree nor disagree",
  "Slightly agree",
  "Moderately agree",
  "Strongly agree",
  "Very strongly agree"
]

const scale_emotion = [
  "Strongly disagree",
  "Disagree",
  "Neither agree nor disagree",
  "Agree",
  "Strongly agree",
]

const questions_cog = [
  "I would prefer complex to simple problems.",
  "I like to have the responsibility of handling a situation that requires a lot of thinking.",
  "Thinking is not my idea of fun.",
  "I would rather do something that requires little thought than something that is sure to challenge my thinking abilities.",
  "I try to anticipate and avoid situations where there is likely chance I will have to think in depth about something.",
  "I find satisfaction in deliberating hard and for long hours.",
  "I only think as hard as I have to.",
  "I prefer to think about small, daily projects to long-term ones.",
  "I like tasks that require little thought once I've learned them.",
  "The idea of relying on thought to make my way to the top appeals to me.",
  "I really enjoy a task that involves coming up with new solutions to problems.",
  "Learning new ways to think doesn't excite me very much.",
  "I prefer my life to be filled with puzzles that I must solve.",
  "The notion of thinking abstractly is appealing to me.",
  "I would prefer a task that is intellectual, difficult, and important to one that is somewhat important but does not require much thought.",
  "I feel relief rather than satisfaction after completing a task that required a lot of mental effort.",
  "It's enough for me that something gets the job done; I don't care how or why it works.",
  "I usually end up deliberating about issues even when they do not affect me personally."
]

const questions_emo = [
  "I try to anticipate and avoid situations where there is a likely chance of my getting emotionally involved.",
  "Experiencing strong emotions is not something I enjoy very much.",
  "I would rather be in a situation in which I experience little emotion than one which is sure to get me emotionally involved.",
  "I don't look forward to being in situations that others have found to be emotional.",
  "I look forward to situations that I know are less emotionally involving.",
  "I like to be unemotional in emotional situations.",
  "I find little satisfaction in experiencing strong emotions.",
  "I prefer to keep my feelings under check.",
  "I feel relieved rather than fulfilled after experiencing a situation that was very emotional.",
  "I prefer to ignore the emotional aspects of situations rather than getting involved in them.",
  "More often than not, making decisions based on emotions just leads to more errors.",
  "I don't like to have the responsibility of handling a situation that is emotional in nature."
]

const cognition = {
  type: 'survey-likert',
  questions: questions_cog.map((v, index) => ({ prompt: v, name: index, labels: scale_cognition, required: true })),
  data: { type: 'cognition-scale' }
}

const emotion = {
  type: 'survey-likert',
  questions: questions_emo.map((v, index) => ({ prompt: v, name: index, labels: scale_emotion, required: true })),
  data: { type: 'emotion-scale' }
}

const demographics = {
  type: 'survey-html-form',
  html: `
    <div>
      <p> How old are you? <input type="number" name="age" min="18" required /> </p>
      <p> What is your gender?
        <select name="gender" required>
          <option> Man </option>
          <option> Woman </option>
          <option> Other </option>
          <option> I do not want to answer </option>
        </select>
      </p>
      <p> What is the highest degree of education you have attained?
        <select name="education" required>
          <option> Primary </option>
          <option> Secondary </option>
          <option> Undergraduate </option>
          <option> Masters </option>
          <option> Doctorate </option>
        </select>
      </p>
      <p> Do you have any formal education in music?
        <select name="music" required>
          <option> No </option>
          <option> Primary </option>
          <option> Secondary </option>
          <option> Undergraduate </option>
          <option> Masters </option>
          <option> Doctorate </option>
        </select>
      </p>
      <p> Do you have any formal education in visual art?
        <select name="visual" required>
          <option> No </option>
          <option> Primary </option>
          <option> Secondary </option>
          <option> Undergraduate </option>
          <option> Masters </option>
          <option> Doctorate </option>
        </select>
      </p>
    </div>
  `,
  data: { type: 'demographics' }
}

const landing = {
  type: plugin_type,
  choices: ['enter'],
  data: { type: 'landing' },
  stimulus: `
  <div  style="font-size:14px; text-align: left">
  <h2 style="text-align:center"> Welcome! </h2>
  <h4 style="text-align:center"> Instructions </h4>
  <p style="text-align:center"> In the first part of the experiment, you will complete an auditory task and a visual task. You will get specific Instructions
    for the tasks right before they begin. In the second part of the experiment, you will fill in some questionnaires related to
    thinking and feeling, and finally you will answer a few short general questions. </p>
    <div style="display: flex; flex-direction: row">
      <div style="display: flex; flex-direction: column; flex: 1 1; margin-right: 2em">
        <h4> Technical requirements </h4>
        <p> Please use Google Chrome. Any version above 85 is okay. </p>
        <h4> Participation requirements </h4>
        <p> You must be over the age of 18, have normal or corrected-to-normal vision and hearing, and have no cognitive disability. </p>
        <h4> Estimated duration </h4>
        <p> The study should take you about 30-40 minutes to complete. </p>
      </div>
      <div style="display: flex; flex-direction: column; flex: 1 1; margin-left: 2em">
        <h4> Consent and data processing</h4>
        <p> The data from this study will only be used in a project for the course "Human Perception for Information Technology," and nowhere outside
          of that context. Your data is anonymised as soon as you complete the experiment, and will not be connected to your identity.</p>
        <p> You may withdraw your consent to participate in the experiment at any time. You may take a break between blocks and tasks, but not
          in the middle of a trial. </p>
          <h4> Contact </h4>
          <p> If you would like any more information, please reach the team at: siyanai@kth.se </p>
      </div>
    </div>
    <h2 style="text-align:center"> By pressing the ENTER key, you certify that you agree to these conditions, meet the participation requirements,
      and would like to proceed to the experiment. </h2>
  </div>
  `
}

const timeline = [
  landing,
  welcome,
  ...blocks.flat(Infinity),
  proceed,
  cognition,
  emotion,
  demographics
]

const download = (filename, text) => {
  const element = document.createElement('a')
  element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(text))
  element.setAttribute('download', filename)
  element.style.display = 'none'

  document.body.appendChild(element)
  element.click()

  document.body.removeChild(element)
}

const random = (l) => [...Array(l)].map(i => (~~(Math.random() * 36)).toString(36)).join('')

const jsonURL = 'https://jsonbox.io/box_789896c77aa8ed2b2f34'

jsPsych.init({
  timeline,
  on_finish: () => {
    const data = jsPsych.data.get().json()
    download(`results-trials_${random(10)}.json`, data)
    fetch(jsonURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: data
    }).then(() => alert('Data successfully saved'))
      .catch(e => {
        alert('Could not save data :(')
        console.error(e)
      })
  }
})
