## How to use

1. Clone this repository
2. Open the repo folder and find index.html
3. Open index.html, preferably with Chrome. I have not tested other browsers.
4. Proceed with experiment
5. The files with results will get automatically saved to the default downloads directory. Don't forget to send these files to your KTH email or otherwise make absolutely sure we have them.
6. To run again from the start, just open index.html again.